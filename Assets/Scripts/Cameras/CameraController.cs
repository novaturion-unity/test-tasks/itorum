﻿using UnityEngine;

namespace Itorum.Cameras
{
	public class CameraController : MonoBehaviour
	{
		[Range(1, 10)]
		public float sensitivity = 1f;

		public Camera targetCamera = null;

		/// <summary>
		/// Sensitivity divided by 100
		/// </summary>
		private float reducedSensitivity;

		private Managers.ActionsManager actionsManager = null;

		private void Start()
		{
			Debug.Assert(targetCamera != null, "TargetCamera is null");

			actionsManager = Managers.ActionsManager.Instance;

			if (actionsManager)
			{ actionsManager.Actions.Mouse.Rotation.performed += context => Rotate(context.ReadValue<Vector2>()); }
			else
			{ Debug.LogError("ActionsManager is null"); }
		}

		public void Rotate(Vector2 rotationDelta)
		{
			if (!targetCamera ||
				!this.gameObject.activeSelf ||
				Cursor.lockState != CursorLockMode.Locked)
			{ return; }

			reducedSensitivity = sensitivity / 100;

			Vector3 newRotation = targetCamera.transform.rotation.eulerAngles;
			newRotation.x -= rotationDelta.y * reducedSensitivity;
			newRotation.y += rotationDelta.x * reducedSensitivity;

			newRotation.x = newRotation.x > 180 ? newRotation.x - 360 : newRotation.x;
			newRotation.x = Mathf.Clamp(newRotation.x, -89, 89);

			targetCamera.transform.eulerAngles = newRotation;
		}
	}
}