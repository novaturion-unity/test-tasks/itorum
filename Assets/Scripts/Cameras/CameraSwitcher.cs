﻿using System.Collections.Generic;
using UnityEngine;

namespace Itorum.Cameras
{
	public class CameraSwitcher : MonoBehaviour
	{
		public List<Camera> cameras = null;

		public Camera currentCamera { get; private set; }

		private void Start()
		{
			Debug.Assert(cameras != null, "Cameras list is null");

			currentCamera = Camera.main;
		}

		public void SetCamera(int index, Camera camera)
		{
			if (index < 0 || index >= cameras.Count)
			{
				Debug.LogError("Index is out of range\nIndex: " + index + "\tRange: [0, " + (cameras.Count - 1) + "]");
				return;
			}

			cameras[index] = camera;
		}

		public void Switch(int index)
		{
			if (!currentCamera)
			{
				Debug.LogError("CurrentCamera is null");
				return;
			}

			if (cameras == null)
			{
				Debug.LogError("Cameras list is null");
				return;
			}

			if (index < 0 || index >= cameras.Count)
			{
				Debug.LogError("Index is out of range\nIndex: " + index + "\tRange: [0, " + (cameras.Count - 1) + "]");
				return;
			}

			if (index == cameras.IndexOf(currentCamera))
			{ return; }

			if (!cameras[index])
			{ return; }

			currentCamera.gameObject.SetActive(false);
			currentCamera = cameras[index];
			currentCamera.gameObject.SetActive(true);
		}
	}
}