﻿using UnityEngine;

namespace Itorum.Actors
{
	public class Spawner : MonoBehaviour
	{
		public GameObject airplanePrefab = null;
		public GameObject missilePrefab = null;

		public Vector3 airplaneSpawnPosition = Vector3.zero;
		public Vector3 airplaneSpawnRotation = Vector3.zero;
		public Vector3 missileSpawnPosition = Vector3.zero;
		public Vector3 missileSpawnRotation = Vector3.zero;

		private Managers.ActorsManager actorsManager = null;

		void Start()
		{
			actorsManager = Managers.ActorsManager.Instance;

			Debug.Assert(airplanePrefab, "AirplanePrefab is null");
			Debug.Assert(missilePrefab, "MissilePrefab is null");
		}

		public GameObject SpawnAirplane()
		{ return Instantiate(airplanePrefab, airplaneSpawnPosition, Quaternion.Euler(airplaneSpawnRotation)); }

		public GameObject SpawnMissile()
		{ return Instantiate(missilePrefab, missileSpawnPosition, Quaternion.Euler(missileSpawnRotation)); }
	}
}