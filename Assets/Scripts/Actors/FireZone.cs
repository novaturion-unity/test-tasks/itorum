﻿using UnityEngine;

namespace Itorum.Actors
{
	public class FireZone : MonoBehaviour
	{
		/// <summary>
		/// What objects can interact with zone
		/// </summary>
		public LayerMask layersToCollide = Physics.DefaultRaycastLayers;

		private Managers.ActorsManager actorsManager = null;

		private void Start()
		{ actorsManager = Managers.ActorsManager.Instance; }

		private void OnTriggerEnter(Collider other)
		{
			if ((layersToCollide & (1 << other.gameObject.layer)) != 0)
			{ actorsManager?.OnAirplaneEnteredFireZone(); }
		}

		private void OnTriggerExit(Collider other)
		{
			if ((layersToCollide & (1 << other.gameObject.layer)) != 0)
			{ actorsManager?.OnAirplaneExitedFireZone(); }
		}
	}
}