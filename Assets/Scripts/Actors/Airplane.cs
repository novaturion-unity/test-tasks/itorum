﻿using UnityEngine;

namespace Itorum.Actors
{
	[RequireComponent(typeof(Rigidbody))]
	public class Airplane : MonoBehaviour
	{
		public float speed = 93.3f;
		/// <summary>
		/// Airplane speed after missile hit in km/h
		/// </summary>
		public float crashSpeed = 124.4f;

		/// <summary>
		/// Airplane rotation after missile hit
		/// </summary>
		public Vector3 crashRotation = Vector3.zero;

		/// <summary>
		/// Attached rigidbody
		/// </summary>
		public new Rigidbody rigidbody = null;

		/// <summary>
		/// Attached camera
		/// </summary>
		public new Camera camera = null;

		/// <summary>
		/// What objects can hit an airplane
		/// </summary>
		public LayerMask layersToCollide = Physics.DefaultRaycastLayers;

		private bool isHitted = false;

		/// <summary>
		/// Airplane quaternion rotation after missile hit
		/// </summary>
		private Quaternion crashRotationEuler = Quaternion.identity;

		private Managers.ActorsManager actorsManager = null;

		private void Start()
		{
			crashRotationEuler = Quaternion.Euler(crashRotation);

			actorsManager = Managers.ActorsManager.Instance;

			if (!rigidbody)
			{ Debug.LogError("Rigidbody is null"); }
			else
			{ rigidbody.velocity = rigidbody.transform.forward * speed; }

			Debug.Assert(camera != null, "Camera is null");
		}

		private void Hit()
		{
			this.transform.rotation = rigidbody.rotation = crashRotationEuler;
			rigidbody.velocity = this.transform.forward * crashSpeed;
			rigidbody.useGravity = true;
		}

		public void Crash()
		{
			actorsManager.OnAirplaneCrashed();
			Destroy(this.gameObject);
		}

		private void OnCollisionEnter(Collision other)
		{
			if ((layersToCollide & (1 << other.gameObject.layer)) != 0)
			{
				if (!isHitted)
				{
					isHitted = true;
					Hit();
				}
				else
				{ Crash(); }
			}
		}
	}
}