﻿using UnityEngine;

namespace Itorum.Managers
{
	public class ActorsManager : MonoBehaviour
	{
		public static ActorsManager Instance { get; private set; }

		/// <summary>
		/// Latest instantiated airplane
		/// </summary>
		public GameObject airplaneInstance { get; private set; }
		/// <summary>
		/// Latest instantiated missile
		/// </summary>
		public GameObject missileInstance { get; private set; }

		public Actors.Spawner actorsSpawner = null;

		private UIManager uiManager = null;

		private Actors.Missile missile = null;
		private Actors.Airplane airplane = null;

		private void Awake()
		{
			if (Instance == null)
			{ Instance = this; }
			else if (Instance == this)
			{ Destroy(this.gameObject); }
		}

		private void Start()
		{
			uiManager = Managers.UIManager.Instance;
			SpawnAirplane();
		}

		public void SpawnMissile()
		{
			missileInstance = actorsSpawner.SpawnMissile();
			missile = missileInstance.GetComponent<Actors.Missile>();
			missile.ObjectToHit = airplaneInstance;

			uiManager.SetMissileCamera(missile.camera);
		}

		public void SpawnAirplane()
		{
			airplaneInstance = actorsSpawner.SpawnAirplane();
			airplane = airplaneInstance.GetComponent<Actors.Airplane>();

			uiManager.SetAirplaneCamera(airplane.camera);
		}

		public void LaunchMessile()
		{ missile.Launch(); }

		public void OnAirplaneEnteredFireZone()
		{ uiManager.SetValidity(true); }

		public void OnAirplaneExitedFireZone()
		{ uiManager.SetValidity(false); }

		public void OnMissileHitted()
		{
			uiManager.ShowHittedMessage();
			uiManager.SetCameraControlsVisibility(false);
			uiManager.ActivateAirplaneCamera();
		}

		public void OnAirplaneCrashed()
		{
			uiManager.SetLaunchControlsVisibility(true);
			uiManager.ActivateDefaultCamera();
			SpawnAirplane();
		}
	}
}